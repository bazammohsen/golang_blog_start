package router

import (
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
)

func RequestDataLogger() gin.HandlerFunc {
	return func(c *gin.Context) {
		buf, _ := ioutil.ReadAll(c.Request.Body)
		rdr1 := ioutil.NopCloser(bytes.NewBuffer(buf))
		rdr2 := ioutil.NopCloser(bytes.NewBuffer(buf)) //We have to create a new Buffer, because rdr1 will be read.
		newLined := strings.Replace(ReadBody(rdr1), "&", "\n       ", -1)
		// newLined := re.ReplaceAllString(ReadBody(rdr1), "&")
		fmt.Printf("================================================\n data:\n       %s\n", newLined)
		fmt.Println("----------------")
		c.Request.Body = rdr2
		c.Next()
	}
}

func ReadBody(reader io.Reader) string {
	buf := new(bytes.Buffer)
	buf.ReadFrom(reader)

	s := buf.String()
	return s
}
func RequestDetailsLogger() gin.HandlerFunc {

	return gin.LoggerWithFormatter(func(param gin.LogFormatterParams) string {
		// your custom format
		return fmt.Sprintf(" client ip:  %s\n req-time:  [%s] \n method:  %s \n path-:  %s \n protocol:  %s\n status:  %d \n latency:  %s \n user-agent:  %s\n error-message:  %s\n^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n",
			param.ClientIP,
			param.TimeStamp.Format(time.RFC1123),
			param.Method,
			param.Path,
			param.Request.Proto,
			param.StatusCode,
			param.Latency,
			param.Request.UserAgent(),
			param.ErrorMessage,
		)
	})

}
