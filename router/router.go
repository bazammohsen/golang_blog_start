package router

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/bazammohsen/golang_blog_start/config"
	"gitlab.com/bazammohsen/golang_blog_start/db"
	"go.uber.org/fx"
)

type Router interface {
	RunRouter()
	GetRouter() *gin.Engine
}

type router struct {
	router          *gin.Engine
	databaseHandler db.DBHandler
	config          config.Configs
}

func NewRouter(databaseHandler db.DBHandler, config config.Configs) Router {
	instance := &router{}
	instance.databaseHandler = databaseHandler
	instance.config = config
	router := gin.Default()
	if config.GetEnv() == "PROD" {
		gin.SetMode(gin.ReleaseMode)

	} else {
		gin.ForceConsoleColor()
		router.Use(RequestDataLogger())
		router.Use(RequestDetailsLogger())
	}

	instance.router = router
	return instance
}
func (r *router) RunRouter() {
	r.router.Run(":" + r.config.GetString(config.ROUTER_PORT))
}
func (r *router) GetRouter() *gin.Engine {
	return r.router
}

var Module = fx.Options(
	fx.Provide(NewRouter),
)
