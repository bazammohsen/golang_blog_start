package config

import (
	"strings"

	"github.com/spf13/viper"
	"go.uber.org/fx"
)

type configs struct {
	viper *viper.Viper
}

type Configs interface {
	GetString(name string) string
	GetDSN() string
	GetEnv() string
}

func (c *configs) GetString(name string) string {
	return c.viper.GetString(name)
}
func (c *configs) GetDSN() string {
	return c.viper.GetString(DSN)
}
func (c *configs) GetEnv() string {
	return c.viper.GetString(ENV)
}

const FileName = "config"

const FilePath = "../config/"

//since viper does not accept absolute path  we have this relative path for our functional test

func NewConfig() Configs {
	instance := &configs{}
	v := viper.New()

	v.SetConfigType("yml")

	v.SetConfigName(FileName)

	v.AddConfigPath(FilePath)

	err := v.ReadInConfig()

	if err != nil {
		panic("can not read config file" + err.Error())
	}

	v.AutomaticEnv()

	v.SetEnvPrefix("golang_blog")

	v.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))

	_ = v.AllSettings()
	instance.viper = v
	return instance
}

func GetConfig(viper *viper.Viper, configName string) string {
	return viper.GetString(configName)
}

var Module = fx.Options(
	fx.Provide(NewConfig),
)
