package utils

import (
	"fmt"
	"sync"
	"time"
)

type item struct {
	price    int
	category string
}

func TestGoRoutinPipeline() {
	// c := gen(
	// 	item{
	// 		price:    8,
	// 		category: "shirt",
	// 	},
	// 	item{
	// 		price:    20,
	// 		category: "shoe",
	// 	},
	// 	item{
	// 		price:    24,
	// 		category: "shoe",
	// 	},
	// 	item{
	// 		price:    4,
	// 		category: "drink",
	// 	},
	// 	item{
	// 		price:    12,
	// 		category: "shirt",
	// 	},
	// )
	// out := discount(c)
	// for processed := range out {
	// 	fmt.Println("Category:", processed.category, "Price:", processed.price)
	// }
}
func discount(done chan bool, items <-chan item) <-chan item {
	out := make(chan item)
	go func() {
		defer close(out)
		for i := range items {
			time.Sleep(time.Second * 1)
			if i.category == "shoe" {
				i.price /= 2
			}
			select {
			case out <- i:
			case <-done:
				return
			}
			out <- i
		}
	}()
	return out
}
func gen(items ...item) <-chan item {
	out := make(chan item, len(items))
	for _, i := range items {
		out <- i
	}
	close(out)
	return out
}

////////////////////////////////////////////

func TestFanInFanOut() {
	done := make(chan bool)
	defer close(done)
	c := gen(
		item{
			price:    8,
			category: "shirt",
		},
		item{
			price:    20,
			category: "shoe",
		},
		item{
			price:    24,
			category: "shoe",
		},
		item{
			price:    4,
			category: "drink",
		},
		item{
			price:    12,
			category: "shirt",
		},
	)
	c1 := discount(done, c)
	c2 := discount(done, c)
	out := fanIn(done, c1, c2)
	for processed := range out {
		fmt.Println("Category:", processed.category, "Price:", processed.price)
	}
}
func fanIn(done <-chan bool, channels ...<-chan item) <-chan item {
	var wg sync.WaitGroup
	out := make(chan item)
	output := func(c <-chan item) {
		defer wg.Done()
		for i := range c {
			select {
			case out <- i:
			case <-done:
				return
			}
			out <- i
		}
	}
	wg.Add(len(channels))
	for _, c := range channels {
		go output(c)
	}
	go func() {
		wg.Wait()
		close(out)

	}()
	return out
}
