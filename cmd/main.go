package main

import (
	"context"

	"gitlab.com/bazammohsen/golang_blog_start/db"
	"gitlab.com/bazammohsen/golang_blog_start/moduleContainer"
	"gitlab.com/bazammohsen/golang_blog_start/server"
	"go.uber.org/fx"
)

func main() {
	// utilities.TestGoRoutinPipeline()
	// utils.TestFanInFanOut()
	// cryptography.Crrypto()
	// go analyze.Analyze()
	app := fx.New(
		moduleContainer.Module,
		fx.Invoke(register),
	)
	app.Run()

}

func register(lifecycle fx.Lifecycle, server server.Server, db db.DBHandler) {

	lifecycle.Append(
		fx.Hook{
			OnStart: func(context.Context) error {
				db.Initialize()
				server.Start()
				return nil
			},
			OnStop: func(context.Context) error {
				server.Stop()
				return nil
			},
		},
	)
}
