package moduleContainer

import (
	"gitlab.com/bazammohsen/golang_blog_start/config"
	"gitlab.com/bazammohsen/golang_blog_start/db"
	"gitlab.com/bazammohsen/golang_blog_start/internal/handlers"
	"gitlab.com/bazammohsen/golang_blog_start/job"
	"gitlab.com/bazammohsen/golang_blog_start/router"
	"gitlab.com/bazammohsen/golang_blog_start/server"
	"gitlab.com/bazammohsen/golang_blog_start/zapLogger"
	"go.uber.org/fx"
)

var Module = fx.Options(
	router.Module,
	config.Module,
	job.Module,
	db.Module,
	zapLogger.Module,
	handlers.Module,
	server.Module,
)
