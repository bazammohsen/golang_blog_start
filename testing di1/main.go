package main

import (
	"fmt"
	"go.uber.org/fx"
	"log"
)

type CustomString string
type MyAwesomeLogger struct {
	Prefix string
}

// LoggerInterface interfaces logging
type LoggerInterface interface {
	LogMe(str string) string
}

// LogMe logs given string
func (lg *MyAwesomeLogger) LogMe(data string) string {
	return fmt.Sprintf("%v: %v\n", lg.Prefix, data)
}

// ProvideString provides a string
func ProvideString() CustomString {
	var str CustomString = "hello world"
	return str
}

// ProvideLogger provides a logger
func ProvideLogger() LoggerInterface {
	return &MyAwesomeLogger{Prefix: "Konichiwa"}
}

// RunMe should be run (logger and str are injected automatically by go-fx)
func RunMe(logger LoggerInterface, str CustomString) {
	logData := logger.LogMe("Hello Dependency Injection Works")
	fmt.Println(logData)
	fmt.Println("Received string from DI", str)
}
func main() {
	log.Println("Starting Application")
	fx.New(
		fx.Provide(ProvideString),
		fx.Provide(ProvideLogger),
		fx.Invoke(RunMe),
	).Run()
}
