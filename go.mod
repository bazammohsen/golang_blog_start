module gitlab.com/bazammohsen/golang_blog_start

go 1.16

require (
	github.com/gin-gonic/gin v1.7.2
	github.com/go-playground/validator/v10 v10.8.0 // indirect
	github.com/google/pprof v0.0.0-20210726183535-c50bf4fe5303 // indirect
	github.com/ianlancetaylor/demangle v0.0.0-20210724235854-665d3a6fe486 // indirect
	github.com/lib/pq v1.10.2 // indirect
	github.com/logrusorgru/aurora v2.0.3+incompatible // indirect
	github.com/maoueh/zap-pretty v0.2.2 // indirect
	github.com/mattn/go-isatty v0.0.13 // indirect
	github.com/spf13/viper v1.8.1
	github.com/ugorji/go v1.2.6 // indirect
	go.uber.org/atomic v1.9.0 // indirect
	go.uber.org/dig v1.11.0 // indirect
	go.uber.org/fx v1.13.1
	go.uber.org/multierr v1.7.0 // indirect
	go.uber.org/zap v1.18.1
	google.golang.org/protobuf v1.27.1 // indirect
	gorm.io/driver/mysql v1.1.1
	gorm.io/driver/postgres v1.1.0
	gorm.io/gorm v1.21.11
)
