package server

import (
	"log"

	"gitlab.com/bazammohsen/golang_blog_start/config"
	"gitlab.com/bazammohsen/golang_blog_start/db"
	"gitlab.com/bazammohsen/golang_blog_start/internal/handlers"
	"gitlab.com/bazammohsen/golang_blog_start/job"
	"gitlab.com/bazammohsen/golang_blog_start/router"
	"go.uber.org/fx"
	"go.uber.org/zap"
)

// Server extrude the public methods of server object
type Server interface {
	Start()
	Stop()
}

type server struct {
	zapLogger     *zap.SugaredLogger
	router        router.Router
	dbHandler     db.DBHandler
	routeHandlers handlers.Handlers
	conf          config.Configs
	queue         job.JobQueue
}

// NewServer instantiate a new server
func NewServer(
	databaseHandler db.DBHandler,
	config config.Configs,
	q job.JobQueue,
	zapLogger *zap.SugaredLogger,
	router router.Router,
	handlers handlers.Handlers,
) Server {
	log.Println("Instantiate server instance")
	instance := &server{}

	instance.zapLogger = zapLogger

	instance.routeHandlers = handlers

	instance.router = router

	instance.dbHandler = databaseHandler

	instance.conf = config

	instance.queue = q

	return instance
}

func (s *server) Start() {
	s.zapLogger.Infow("Start running server")

	go s.router.RunRouter()
	s.zapLogger.Infow("running router")

}

func (s *server) Stop() {
	s.zapLogger.Infow("Stop running server")
}

var Module = fx.Options(
	fx.Provide(NewServer),
)
