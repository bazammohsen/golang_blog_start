package models

import "gorm.io/gorm"

type Post struct {
	gorm.Model
	ID                uint
	Title             string
	ShortDescriptions string
	Content           string
	Image             string
	User              User
	UserID            uint
}
