package models

import "gorm.io/gorm"

type User struct {
	gorm.Model
	ID       uint
	Name     string
	Username string
	Password string
	IsAdmin  bool
}
