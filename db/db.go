package db

import (
	"fmt"
	"log"

	"gitlab.com/bazammohsen/golang_blog_start/config"
	"gitlab.com/bazammohsen/golang_blog_start/models"
	"go.uber.org/fx"
	"go.uber.org/zap"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

// DBHandler extrude the public methods of handler object
type DBHandler interface {
	Initialize()
	DropTable()
	GetDb() *gorm.DB
}

type dbhandlerStruct struct {
	timeout int
	retry   int
	conf    config.Configs
	db      *gorm.DB
}

// NewHandler instantiate a new handler
func NewHandler(c config.Configs, z *zap.SugaredLogger) DBHandler {
	instance := &dbhandlerStruct{
		timeout: 3,
		retry:   2,
	}
	instance.conf = c

	// newLogger := logger.New(
	// 	log.New(os.Stdout, "\r\n", log.LstdFlags), // io writer
	// 	logger.Config{
	// 		SlowThreshold:             time.Second,   // Slow SQL threshold
	// 		LogLevel:                  logger.Silent, // Log level
	// 		IgnoreRecordNotFoundError: true,          // Ignore ErrRecordNotFound error for logger
	// 		Colorful:                  false,         // Disable color
	// 	},
	// )

	db, err := gorm.Open(postgres.Open(c.GetDSN()))
	// db, err := gorm.Open(postgres.Open(c.GetDSN()), &gorm.Config{
	// 	Logger: newLogger,
	// })

	if err != nil {
		fmt.Println(err.Error())
		panic(err)
	}
	instance.db = db
	z.Info("connected to db")
	return instance
}

func (h *dbhandlerStruct) Initialize() {

	setup(h.db)
	log.Println("Initialized DB")
}

func (h *dbhandlerStruct) DropTable() {
	log.Println("Drop database table")
}
func (d *dbhandlerStruct) GetDb() *gorm.DB {
	return d.db
}

func setup(db *gorm.DB) {
	db.AutoMigrate(&models.Post{}, &models.User{})
	// seed(db)
}

func seed(db *gorm.DB) {
	users := []models.User{
		{Name: "General", Username: "user1UserName", IsAdmin: false, Password: "user1Password"},
		{Name: "General2", Username: "user2UserName", IsAdmin: false, Password: "user2Password"},
		{Name: "General3", Username: "user3UserName", IsAdmin: false, Password: "user3Password"},
		{Name: "General4", Username: "user4UserName", IsAdmin: false, Password: "user4Password"},
		{Name: "General5", Username: "user5UserName", IsAdmin: true, Password: "user5Password"},
	}
	for _, c := range users {
		db.Create(&c)
	}

}

var Module = fx.Options(
	fx.Provide(NewHandler),
)
