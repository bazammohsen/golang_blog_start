package zapLogger

import (
	"go.uber.org/fx"
	"go.uber.org/zap"
)

func NewZapLogger() *zap.SugaredLogger {
	logger, _ := zap.NewProduction()
	slogger := logger.Sugar()
	defer logger.Sync() // flushes buffer, if any
	return slogger
}

var Module = fx.Options(
	fx.Provide(NewZapLogger),
)
