package user

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/bazammohsen/golang_blog_start/db"
	"go.uber.org/fx"
	"go.uber.org/zap"
)
type SignupModel struct{
    Email string `json:"email" form:"email" binding:"required"`
    Password string `json:"password" form:"password" binding:"required"`
}
type UserServices interface {
	Signup(c *gin.Context)
}

type userServices struct {
	dbHandler db.DBHandler
	logger    *zap.SugaredLogger
}

func NewUserServices(dbHandler db.DBHandler, logger *zap.SugaredLogger) UserServices {
	instance := &userServices{}
	instance.dbHandler = dbHandler
	instance.logger = logger
	return instance
}

func (userService *userServices) Signup(c *gin.Context) {
var signupModel SignupModel

if err := c.ShouldBind(&signupModel); err != nil {
	c.JSON(http.StatusBadRequest, gin.H{"error": "field validation failed"})
	return
}

// Data is ok
	c.JSON(200, gin.H{
		"message": "every little thing, is gonna be alright, new one specially",
		"email":signupModel.Email,
	})
}


var UserServicesModule = fx.Options(
	fx.Provide(NewUserServices),
)
