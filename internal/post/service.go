package post

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/bazammohsen/golang_blog_start/db"
	"go.uber.org/fx"
	"go.uber.org/zap"
)

type PostServices interface {
	GetAllPosts(c *gin.Context)
	GetSinglePost(c *gin.Context)
	CreatPlogPost(c *gin.Context)
}

type postServices struct {
	dbHandler db.DBHandler
	logger    *zap.SugaredLogger
}

func NewPostServices(dbHandler db.DBHandler, logger *zap.SugaredLogger) PostServices {
	instance := &postServices{}
	instance.dbHandler = dbHandler
	instance.logger = logger
	return instance
}

func (postService *postServices) GetAllPosts(c *gin.Context) {
	postService.logger.Infow("get all the posts")
	c.JSON(200, gin.H{
		"message": "every litle thing, is gonna be alright",
	})
}
func (postService *postServices) GetSinglePost(c *gin.Context) {
	postService.logger.Infow("get single post, id: " + c.Param("id"))
	c.JSON(200, gin.H{
		"message": "every litle thing, is gonna be alright,at least one of them",
	})
}
func (postService *postServices) CreatPlogPost(c *gin.Context) {
	postService.logger.Infow("get single post, id: " + c.Param("id"))
	c.JSON(200, gin.H{
		"message": "every litle thing, is gonna be alright,spicially the new one",
	})
}

var PostServicesModule = fx.Options(
	fx.Provide(NewPostServices),
)
