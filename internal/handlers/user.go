package handlers

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/bazammohsen/golang_blog_start/internal/user"
	"gitlab.com/bazammohsen/golang_blog_start/router"

	"go.uber.org/fx"
)

type userHandlers struct {
	router       router.Router
	userServices user.UserServices
}
type UserHandlers interface {
	InitializeUserHandlers()
}

func NewUserHandler(
	router router.Router,
	userServices user.UserServices,
) UserHandlers {

	instance := &userHandlers{}
	instance.userServices = userServices
	instance.router = router

	return instance
}

func (userHandler *userHandlers) InitializeUserHandlers() {

	router := userHandler.router.GetRouter()

	router.POST("/signup", func(c *gin.Context) {
		userHandler.userServices.Signup(c)
	})

}

var UserHandlerModule = fx.Options(
	user.UserServicesModule,
	fx.Provide(NewUserHandler),
)
