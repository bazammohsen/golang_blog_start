package handlers

import (
	"go.uber.org/fx"
	"go.uber.org/zap"
)

type Handlers interface {
	InitializeHandlers()
}

type handlers struct {
	logger       *zap.SugaredLogger
	postsHandler PostHandlers
	userHandlers UserHandlers
}

func NewHandlers(p PostHandlers, u UserHandlers, zapLogger *zap.SugaredLogger) Handlers {
	instance := &handlers{}
	instance.logger = zapLogger
	instance.postsHandler = p
	instance.userHandlers = u

	instance.InitializeHandlers()
	zapLogger.Infow("initialized Posts Handlers")
	return instance
}
func (h *handlers) InitializeHandlers() {
	h.postsHandler.InitializePostHandlers()
	h.logger.Infow("initialized Posts Handlers")
	h.userHandlers.InitializeUserHandlers()
	h.logger.Infow("initialized User Handlers")
}

var Module = fx.Options(
	PostsHandlerModule,
	UserHandlerModule,
	fx.Provide(NewHandlers),
)
