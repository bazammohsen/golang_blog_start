package handlers

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/bazammohsen/golang_blog_start/internal/post"
	"gitlab.com/bazammohsen/golang_blog_start/router"
	"go.uber.org/fx"
)

type postHandlers struct {
	router       router.Router
	postServices post.PostServices
}
type PostHandlers interface {
	InitializePostHandlers()
}

func NewPostHandler(
	router router.Router,
	postServices post.PostServices,
) PostHandlers {

	instance := &postHandlers{}
	instance.postServices = postServices
	instance.router = router

	return instance
}

func (postHandler *postHandlers) InitializePostHandlers() {

	router := postHandler.router.GetRouter()

	router.GET("/posts", func(c *gin.Context) {
		postHandler.postServices.GetAllPosts(c)
	})
	router.GET("/posts/:id", func(c *gin.Context) {
		postHandler.postServices.GetSinglePost(c)
	})
	router.POST("/posts", func(c *gin.Context) {
		postHandler.postServices.CreatPlogPost(c)
	})

}

var PostsHandlerModule = fx.Options(
	post.PostServicesModule,
	fx.Provide(NewPostHandler),
)
